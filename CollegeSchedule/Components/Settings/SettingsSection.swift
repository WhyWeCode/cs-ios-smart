struct SettingsSection {
    public var header: String? = nil
    public var footer: String? = nil
    public let items: [SettingsRow]
}
